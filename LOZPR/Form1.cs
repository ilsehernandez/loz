﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOZPR.Classes;
using LOZPR.Classes.Characters;
using LOZPR.Classes.Items;
using LOZPR.Classes.Visuals;

namespace LOZPR
{
    
    public partial class Form1 : Form
    {
        static int puntaje = 0;
        static Hero link = new Hero(new Coordenadas(0, 0), "", "primer movimiento", 0, new Sizex(0,0));
        static List<Obstacle> obstacles = new List<Obstacle>();
        static List<Weapon> weapons = new List<Weapon>();
        static List<Enemy> enemies = new List<Enemy>();
        static MovementLimits limits = new MovementLimits(400, 600, obstacles, weapons, enemies);

        public Form1()
        {
            InitializeComponent();
            link.Coordenadas = new Coordenadas(pictureBox1.Top, pictureBox1.Left);//Aqui debuggie
            link.Size = new Sizex(pictureBox1.Height, pictureBox1.Width);//Aqui tambien
            obstacles.Add(new Obstacle(new Coordenadas(pictureBox2.Top, pictureBox2.Left),
                new Sizex(pictureBox2.Height, pictureBox2.Width)));//And so on
            obstacles.Add(new Obstacle(new Coordenadas(pictureBox3.Top, pictureBox3.Left),
                new Sizex(pictureBox3.Height, pictureBox3.Width)));
            weapons.Add(new Weapon(5, new Coordenadas(pictureBox4.Top, pictureBox4.Left),
                new Sizex(pictureBox4.Height, pictureBox4.Width), false));
            enemies.Add(new Enemy(new Coordenadas(pictureBox5.Top, pictureBox5.Left),
                new Sizex(pictureBox5.Height, pictureBox5.Width), 100,true,"1"));
            obstacles.Add(new Obstacle(new Coordenadas(pictureBox6.Top, pictureBox6.Left), 
                new Sizex(pictureBox6.Height, pictureBox6.Width)));
            obstacles.Add(new Obstacle(new Coordenadas(pictureBox7.Top, pictureBox7.Left),
                new Sizex(pictureBox7.Height, pictureBox7.Width)));
            obstacles.Add(new Obstacle(new Coordenadas(pictureBox8.Top, pictureBox8.Left),
                new Sizex(pictureBox8.Height, pictureBox8.Width)));
            enemies.Add(new Enemy(new Coordenadas(pictureBox9.Top,pictureBox9.Left),
                new Sizex(pictureBox9.Height, pictureBox9.Width), 200, true,"2"));
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Up)
            {
                link.Mover("Up", limits);
                label2.Text = link.MovValido;
                if (link.Equipada != null)
                {
                    pictureBox4.Visible = !link.Equipada.OnUse;
                    limits.RemoveHW(link);
                }
                pictureBox1.Left = link.Coordenadas.Left;
                pictureBox1.Top = link.Coordenadas.Top;
            }
            else if (e.KeyCode == Keys.Down)
            {
                link.Mover("Down", limits);
                label2.Text = link.MovValido;
                if (link.Equipada != null)
                {
                    pictureBox4.Visible = !link.Equipada.OnUse;
                    limits.RemoveHW(link);
                }
                pictureBox1.Left = link.Coordenadas.Left;
                pictureBox1.Top = link.Coordenadas.Top;
            }
            else if (e.KeyCode == Keys.Left)
            {
                link.Mover("Left", limits);
                label2.Text = link.MovValido;
                if (link.Equipada != null)
                {
                    pictureBox4.Visible = !link.Equipada.OnUse;
                    limits.RemoveHW(link);
                }
                pictureBox1.Left = link.Coordenadas.Left;
                pictureBox1.Top = link.Coordenadas.Top;
            }
            else if (e.KeyCode == Keys.Right)
            {
                link.Mover("Right", limits);
                label2.Text = link.MovValido;
                if (link.Equipada != null)
                {
                    pictureBox4.Visible = !link.Equipada.OnUse;
                    limits.RemoveHW(link);
                }
                pictureBox1.Left = link.Coordenadas.Left;
                pictureBox1.Top = link.Coordenadas.Top;
            }
            else if (e.KeyCode == Keys.A)
            {
                link.Atacar(limits.Enemies);
                label2.Text = link.MovValido;
                int placeE=0;
                bool enemigoM=false;
                foreach(Enemy enemigo in limits.Enemies)
                {
                    label3.Text = enemigo.Vida.ToString();
                    if (enemigo.Vivo)
                    {
                        placeE++;
                    }
                    else
                    {
                        enemigoM = true;
                        puntaje += 10;
                        break;
                    }
                }
                if (enemigoM)
                {
                    if (limits.Enemies.ElementAt(placeE).NumEne == "1")
                        pictureBox5.Visible = !enemigoM;
                    if (limits.Enemies.ElementAt(placeE).NumEne == "2")
                        pictureBox9.Visible = !enemigoM;
                    limits.Enemies.RemoveAt(placeE);
                }
            }
            label4.Text = puntaje.ToString();
            label1.Text = link.Direction;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            Control control = (Control)sender;

            control.Size = new Size(control.Size.Width, control.Size.Height);

            limits.Height = control.Height;
            limits.Width = control.Width;
            label1.Text = limits.Height.ToString();
        }
    }
}
