﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZPR.Classes.Visuals;

namespace LOZPR.Classes
{
    class ZObject
    {
        Coordenadas coordenadas;
        Sizex size;

        public ZObject(Coordenadas coordenadas, Sizex size)
        {
            this.coordenadas = coordenadas;
            this.size = size;
        }

        internal Coordenadas Coordenadas { get => coordenadas; set => coordenadas = value; }
        internal Sizex Size { get => size; set => size = value; }
    }
}
