﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOZPR.Classes.Items
{
    class NoWeaponException:SystemException
    {
        public NoWeaponException() { }

        public NoWeaponException(string Message) : base(Message) { }

        public NoWeaponException(string Message, Exception innerE) : base(Message, innerE) { }

    }
}
