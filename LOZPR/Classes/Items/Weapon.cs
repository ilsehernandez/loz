﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZPR.Classes.Visuals;

namespace LOZPR.Classes.Items
{
    class Weapon:ZObject
    {
        byte damage;
        bool onUse;

        public Weapon(byte damage, Coordenadas coordenadas, Sizex size, bool onUse) : base(coordenadas, size)
        {
            this.damage = damage;
            this.onUse = onUse;
        }

        public byte Damage { get => damage; }
        public bool OnUse { get => onUse; set => onUse = value; }
    }
}
