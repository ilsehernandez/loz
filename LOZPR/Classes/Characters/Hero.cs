﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZPR.Classes.Items;
using LOZPR.Classes.Visuals;

namespace LOZPR.Classes.Characters
{
    class Hero:ZObject
    {
        Weapon equipada;
        string direction;
        string movValido;
        int score;

        public Hero(Coordenadas coordenadas, string direction, string movValido, int score, Sizex size) : base(coordenadas, size)
        {
            equipada = null;
            this.direction = direction;
            this.movValido = movValido;
            this.score = score;
        }

        internal Weapon Equipada { get => equipada; set => equipada = value; }
        public string Direction { get => direction; set => direction = value; }
        public string MovValido { get => movValido; set => movValido = value; }
        public int Score { get => score; set => score = value; }

        public void MostrarInformacion()
        {
            Console.Beep(400, 100);
        }

        public void Atacar(List<Enemy> enemies)
        {
            foreach(Enemy enemigo in enemies)
            {
                if(Direction=="Left"&&((Coordenadas.Left-Size.Width)-enemigo.Coordenadas.Left)==0&&
                    (UInt16)(Coordenadas.Top - enemigo.Coordenadas.Top) == 0)
                {
                    try
                    {
                        enemigo.BajarVida(Equipada);
                    }
                    catch(NoWeaponException ex)
                    {
                        movValido = ex.Message;
                    }
                    
                }
                if (Direction == "Right" && ((Coordenadas.Left + Size.Width) - enemigo.Coordenadas.Left) == 0 &&
                    (UInt16)(Coordenadas.Top - enemigo.Coordenadas.Top) == 0)
                {
                    try
                    {
                        enemigo.BajarVida(Equipada);
                    }
                    catch (NoWeaponException ex)
                    {
                        movValido = ex.Message;
                    }

                }
                if (Direction == "Up" && (Coordenadas.Left - enemigo.Coordenadas.Left) == 0 &&
                    (UInt16)((Coordenadas.Top - Size.Height)- enemigo.Coordenadas.Top) == 0)
                {
                    try
                    {
                        enemigo.BajarVida(Equipada);
                    }
                    catch (NoWeaponException ex)
                    {
                        movValido = ex.Message;
                    }

                }
                if (Direction == "Down" && (Coordenadas.Left - enemigo.Coordenadas.Left) == 0 &&
                    (UInt16)((Coordenadas.Top + Size.Height) - enemigo.Coordenadas.Top) == 0)
                {
                    try
                    {
                        enemigo.BajarVida(Equipada);
                    }
                    catch (NoWeaponException ex)
                    {
                        movValido = ex.Message;
                    }

                }
            }
                
        }

        public void Equipar(Weapon arma)
        {
            arma.OnUse = true;
            equipada = arma;
        }

        public void Mover(string direction, MovementLimits limits)
        {
            this.direction = direction;
            if (direction == "Up")
            {
                try
                {
                    MoveHeight(limits, -1);
                }
                catch (BorderExceptions ex)
                {
                    movValido = ex.Message;
                }
                catch (ColissionException ex)
                {
                    movValido = ex.Message;
                }
                catch(EnemyException ex)
                {
                    movValido = ex.Message;
                }
            }
            if (direction == "Down")
            {
                try
                {
                    MoveHeight(limits, 1);
                }
                catch (BorderExceptions ex)
                {
                    movValido = ex.Message;
                }
                catch (ColissionException ex)
                {
                    movValido = ex.Message;
                }
                catch (EnemyException ex)
                {
                    movValido = ex.Message;
                }

            }
            if (direction == "Right")
            {
                try
                {
                    MoveWidth(limits, 1);
                }
                catch (BorderExceptions ex)
                {
                    movValido = ex.Message;
                }
                catch (ColissionException ex)
                {
                    movValido = ex.Message;
                }
                catch (EnemyException ex)
                {
                    movValido = ex.Message;
                }
            }
            if (direction == "Left")
            {
                try
                {
                    MoveWidth(limits, -1);
                }
                catch (BorderExceptions ex)
                {
                    movValido = ex.Message;
                }
                catch (ColissionException ex)
                {
                    movValido = ex.Message;
                }
                catch (EnemyException ex)
                {
                    movValido = ex.Message;
                }
            }
        }

        public void MoveHeight(MovementLimits limits, int direction)
        {
            if ((Coordenadas.Top+(Size.Width * direction)) < 0 || (Coordenadas.Top + (Size.Height * direction)) >= (limits.Height - 2 * Size.Height))
            {
                throw new BorderExceptions("You're on the border");
            }
            else
            {
                foreach (Obstacle obstacle in limits.Obstacles)
                {
                    if ((UInt16)((Coordenadas.Top + (Size.Height*direction))-obstacle.Coordenadas.Top) == 0 && Coordenadas.Left == obstacle.Coordenadas.Left)
                    {
                        throw new ColissionException("Obstacle on your way");
                    }
                }
                foreach(Weapon weapon in limits.Weapons)
                {
                    if((UInt16)((Coordenadas.Top + (Size.Height * direction)) - weapon.Coordenadas.Top) == 0 && Coordenadas.Left == weapon.Coordenadas.Left)
                    {
                        Equipar(weapon);
                    }
                }
                foreach (Enemy enemy in limits.Enemies)
                {
                    if ((UInt16)((enemy.Coordenadas.Top)- (Coordenadas.Top + (Size.Height * direction))) == 0 && Coordenadas.Left == enemy.Coordenadas.Left)
                    {
                        throw new EnemyException("Enemy on your way");
                    }
                }
                Coordenadas.Top += (Size.Height*direction);
                movValido = "Valid Move";
            }
        }
        
        public void MoveWidth(MovementLimits limits, int direction)
        {
            if ((Coordenadas.Left + (Size.Width * direction)) < 0 || (Coordenadas.Left+(Size.Width*direction))>= (limits.Width - Size.Width))
            {
                throw new BorderExceptions("You're on the border");
            }
            else
            {
                foreach (Obstacle obstacle in limits.Obstacles)
                {
                    if ((Coordenadas.Left + (Size.Width*direction)) == obstacle.Coordenadas.Left && (UInt16)(Coordenadas.Top - obstacle.Coordenadas.Top)==0)
                    {
                        throw new ColissionException("Obstacle on your way");
                    }
                }
                foreach (Weapon weapon in limits.Weapons)
                {
                    if ((UInt16)((Coordenadas.Left + (Size.Width * direction)) - weapon.Coordenadas.Left) == 0 && (UInt16)(Coordenadas.Top - weapon.Coordenadas.Top)==0)
                    {
                        Equipar(weapon);
                    }
                }
                foreach (Enemy enemy in limits.Enemies)
                {
                    if ((Coordenadas.Left + (Size.Width * direction)) == enemy.Coordenadas.Left && (UInt16)(Coordenadas.Top - enemy.Coordenadas.Top) == 0)
                    {
                        throw new EnemyException("Enemy on your way");
                    }
                }
                Coordenadas.Left += (Size.Width*direction);
                movValido = "Valid Move";
            }
        }
    }
}