﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZPR.Classes.Visuals;
using LOZPR.Classes.Items;

namespace LOZPR.Classes.Characters
{
    class Enemy:ZObject
    {
        byte vida;
        bool vivo;
        string numEne;

        public Enemy(Coordenadas coordenadas, Sizex size,byte vida, bool vivo, string numEne) : base(coordenadas,size)
        {
            this.vida = vida;
            this.vivo = vivo;
            this.numEne = numEne;
        }

        public byte Vida { get => vida; }
        public bool Vivo { get => vivo; set => vivo = value; }
        public string NumEne { get => numEne; set => numEne = value; }

        public void MostrarInformacion()
        {

            Console.WriteLine("Soy un enemigo");

        }

        public void BajarVida(Weapon weapon)
        {
            if (weapon == null)
            {
                throw new NoWeaponException("You have no weapon");
            }
            else
            {
                vida -= weapon.Damage;
                if (vida == 0)
                    vivo = false;
            }
            
        }
    }
}
