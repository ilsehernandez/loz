﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOZPR.Classes.Visuals
{
    class ColissionException:SystemException
    {
        public ColissionException() { }

        public ColissionException(string Message) : base(Message) { }

        public ColissionException(string Message, Exception innerE) : base(Message, innerE) { }
    }
}
