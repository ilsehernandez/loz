﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOZPR.Classes.Visuals
{
    class EnemyException:SystemException
    {
        public EnemyException() { }

        public EnemyException(string Message) : base(Message) { }

        public EnemyException(string Message, Exception innerE) : base(Message, innerE) { }

    }
}
