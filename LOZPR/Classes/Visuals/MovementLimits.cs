﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOZPR.Classes.Items;
using LOZPR.Classes.Characters;

namespace LOZPR.Classes.Visuals
{
    class MovementLimits
    {
        int height;
        int width;
        List<Obstacle> obstacles;
        List<Weapon> weapons;
        List<Enemy> enemies;

        public MovementLimits(int height, int width, List<Obstacle> obstacles, List<Weapon> weapons, List<Enemy> enemies)
        {
            this.height = height;
            this.width = width;
            this.obstacles = obstacles;
            this.weapons = weapons;
            this.enemies = enemies;
        }

        public void RemoveHW(Hero hero)
        {
            int place=0;
            bool armaE = false;
            foreach( Weapon weapon in Weapons)
            {
                if (weapon == hero.Equipada)
                {
                    armaE = true;
                    break;
                }
                else
                {
                    place++;
                }
            }
            if (armaE)
            {
                Weapons.RemoveAt(place);
            }
        }

        public int Height { get => height; set => height = value; }
        public int Width { get => width; set => width = value; }
        internal List<Obstacle> Obstacles { get => obstacles; set => obstacles = value; }
        internal List<Weapon> Weapons { get => weapons; set => weapons = value; }
        internal List<Enemy> Enemies { get => enemies; set => enemies = value; }
    }
}
