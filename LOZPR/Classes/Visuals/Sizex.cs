﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOZPR.Classes.Visuals
{
    class Sizex
    {
        int height;
        int width;
        
        public Sizex(int height, int width)
        {
            this.height = height;
            this.width = width;
        }

        public int Height { get => height; set => height = value; }
        public int Width { get => width; set => width = value; }
    }
}
