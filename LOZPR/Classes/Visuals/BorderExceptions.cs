﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOZPR.Classes.Visuals
{
    class BorderExceptions:SystemException
    {
        public BorderExceptions() { }

        public BorderExceptions(string Message) : base(Message) { }

        public BorderExceptions(string Message, Exception innerE) : base(Message, innerE) { }

    }
}
